from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.metrics import dp


class MyApp(App):y
    def build(self):
        return Button(text="App mobile avec Kivy - By Ka", size_hint=(None, None), size=(dp(150), dp(40)))


if __name__ == "__main__":
    MyApp().run()

