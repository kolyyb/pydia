# ![Logo Di@dom](./img_ref/logo_diadom.png) Gestion de stock dialyses à domicile
## Contexte
__Gestion du stock des accessoires de dyalises à la réception (2 lieux de stockage) + calendrier des livraisons.__
### Stack et environnement technique technique:
* Apple
* Pycharm
* Python ^3.11,
* Flask 3.0.0,
* Mysql
* mysql-connector-python 8.2.0
## Modele de données physique

![Modèle de données physique](./img_ref/mdp.png)

> On définit 2 lieux d'entreposage : 
> * Garde-meuble,
> * Domicile
>
> On définit un emplacement d'entreposage pour chaque produit.
> 
> La table ```stock``` recupère donc l'id du produit et l'id du lieu où il est entreposé avec sa quantité:
```sql
CREATE TABLE stocks (quantity INT, id_product INT, id_location INT,
    FOREIGN KEY (id_product) REFERENCES products(id),
    FOREIGN KEY (id_location) REFERENCES locations(id));
```
### v1 du logiciel
* Saisir les stocks
* Saisir les dates de livraisons
* Consulter les sotcks
* Consulter le calendrier des livraisons
### v2 du logiciel
* Incrémenter / décrementer les quantités
* Alerte livraison imminente
* Déplacement du stock de la garde meuble vers le domicile
* Formulaire de saisie des produits livrés
### v3 du logiciel
* Evolution vers une app mobile (Flutter ?)





