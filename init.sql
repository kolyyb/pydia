USE pydia;

-- Create locations table --
CREATE TABLE locations(id INT PRIMARY KEY AUTO_INCREMENT, location_name VARCHAR(30));

-- Create products table --
CREATE TABLE products (id INT PRIMARY KEY AUTO_INCREMENT, product_name VARCHAR(65));

-- Create stocks table with foreign keys --
CREATE TABLE stocks (quantity INT, id_product INT, id_location INT,
    FOREIGN KEY (id_product) REFERENCES products(id),
    FOREIGN KEY (id_location) REFERENCES locations(id));

-- Create deliveries table --
CREATE TABLE deliveries (id INT PRIMARY KEY AUTO_INCREMENT, delivery_date DATETIME);

